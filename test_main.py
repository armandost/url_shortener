import unittest
from main import *

#Integration Testing:



class UnitTest(unittest.TestCase):
    def setUp(self):
        app.config['TESTING'] = True
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'
        self.app = app.test_client()
        with app.app_context():
            db.create_all()


    def tearDown(self):
        with app.app_context():
            db.drop_all()

    def test_registration_and_login(self):
        # Test user registration
        response = self.app.post('/register', data={'username': 'test_user', 'password': 'password123'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, '/login')

        # Test user login
        response = self.app.post('/login', data={'username': 'test_user', 'password': 'password123'})
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response.location, '/shorten')

        # Test if the user is authenticated
        with self.app.session_transaction() as session:
            self.assertTrue(session['_user_id'] is not None)

    def test_generate_short_url(self):
        # Test if the generated short URL has the correct length
        short_url = generate_short_url()
        self.assertEqual(len(short_url), 6)

        # Test if the generated short URL only contains valid characters
        valid_chars = string.ascii_letters + string.digits
        self.assertTrue(all(char in valid_chars for char in short_url))
    def test_url_shortening(self):
        # Test URL shortening functionality
        long_url = "http://example.com"
        response = self.app.post('/shorten', data={'long_url': long_url})
        self.assertEqual(response.status_code, 302)
        # Add assertions to check if the short URL is generated correctly and stored in the database

    def test_url_redirection(self):
        # Test URL redirection functionality
        short_url = "abc123"  # Replace with an existing short URL from the database
        response = self.app.get(f'/{short_url}')
        self.assertEqual(response.status_code, 200)


if __name__ == '__main__':
    unittest.main()