# Importimi i moduleve te kerkuara

import random
import string
from flask import Flask, render_template, redirect, request, flash, url_for, get_flashed_messages
from flask_login import UserMixin, LoginManager, login_user, logout_user, login_required, current_user
from werkzeug.security import generate_password_hash, check_password_hash
from flask_sqlalchemy import SQLAlchemy
import validators
import os
import secrets

# Krijimi i Flask dhe konfigurimi i bazës së të dhënave
app = Flask(__name__)
current_directory = os.path.dirname(os.path.abspath(__file__))

database_path = os.path.join(current_directory, 'shortened_urls.db')
app.config['SQLALCHEMY_DATABASE_URI'] = f'sqlite:///{database_path}'

secret_key = secrets.token_hex(16)
app.config['SECRET_KEY'] = secret_key

db = SQLAlchemy(app)

login_manager = LoginManager(app)
login_manager.login_view = 'login'


class User(db.Model, UserMixin):
    __tablename__ = 'users'  # emri i tabeles ne databaze
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(100), unique=True, nullable=False)
    password_hash = db.Column(db.String(100), nullable=False)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)


class ShortenedURL(db.Model):
    __tablename__ = 'shortened_url'  # emri tabeles ne databaze
    id = db.Column(db.Integer, primary_key=True)
    short_url = db.Column(db.String(10), unique=True, nullable=False)
    long_url = db.Column(db.String(2000), nullable=False)


# Funkision qe gjeneron një URL të shkurtër të rastësishëme me gjatësi të caktuar.

def generate_short_url(length=6):
    chars = string.ascii_letters + string.digits
    short_url = "".join(random.choice(chars) for _ in range(length))
    return short_url


# Konfigurimi i Flask-Login:

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))


#  Definimi i rrugëve dhe funksioneve per te shfaqur templatet:

@app.route("/", methods=["GET"])
def index():
    return render_template("index.html")


@app.route("/shorten", methods=["GET", "POST"])
@login_required
def shorten():
    if request.method == "POST":
        long_url = request.form['long_url']
        if not validators.url(long_url):
            invalid_message = "The URL you entered is not valid. Please make sure to enter a valid URL."
            return render_template("invalid_url.html", invalid_message=invalid_message)

        try:
            with app.app_context():
                existing_url = ShortenedURL.query.filter_by(long_url=long_url).first()
                if existing_url:
                    shortened_url = f"{request.url_root}{existing_url.short_url}"
                else:
                    short_url = generate_short_url()
                    while ShortenedURL.query.filter_by(short_url=short_url).first():
                        short_url = generate_short_url()

                    new_url = ShortenedURL(short_url=short_url, long_url=long_url)
                    db.session.add(new_url)
                    db.session.commit()
                    shortened_url = f"{request.url_root}{short_url}"

                return render_template("shortened.html", shortened_url=shortened_url)

        except Exception as e:
            # Handle the exception and provide an error message to the user
            error_message = "An error occurred while processing your request."
            # Log the error for debugging purposes
            print(f"Error: {e}")
            return render_template("error.html", error_message=error_message)

    return render_template("shorten.html")


@app.route("/register", methods=["GET", "POST"])
def register():
    if request.method == "POST":
        username = request.form['username']
        password = request.form['password']

        existing_user = User.query.filter_by(username=username).first()
        if existing_user:
            flash('Username already exists', 'error')
            return redirect(url_for('register'))

        new_user = User(username=username)
        new_user.set_password(password)
        db.session.add(new_user)
        db.session.commit()

        return redirect(url_for('login'))
    return render_template("register.html", messages=get_flashed_messages())


@app.route("/login", methods=["GET", "POST"])
def login():
    if request.method == "POST":
        username = request.form['username']
        password = request.form['password']

        user = User.query.filter_by(username=username).first()
        if not user or not user.check_password(password):
            flash("Invalid username or password.", 'error')
            return render_template('login.html')

        login_user(user)
        return redirect(url_for('shorten'))

    messages = get_flashed_messages(category_filter=['error'])
    return render_template("login.html", messages=messages)


@app.route("/logout", methods=["POST"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route("/<short_url>")
def redirect_url(short_url):
    try:
        with app.app_context():
            url = ShortenedURL.query.filter_by(short_url=short_url).first()
            if url:
                return redirect(url.long_url)
            else:
                error_message = "The requested URL does not exist."
                return render_template("error.html", error_message=error_message)

    except Exception as e:
        error_message = "An error occurred while processing your request."
        print(f"Error: {e}")
        return render_template("error.html", error_message=error_message)


# Ekzekutohet appi definohet funksioni load_user per te mar  id e perdoruesit i cili regjistrohet paraprakisht

if __name__ == "__main__":
    with app.app_context():
        db.create_all()  # krijimi i tabelave te databazes
    app.config['SERVER_NAME'] = 'armando.com:5000'  # Konfigurimi me një emër serveri të personalizuar
    app.run(debug=True)
